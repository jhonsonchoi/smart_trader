import utils

utils.InitPlusCheck()

# 주식 매도 주문
code = "A900290" # 122630 kodex leverage
quantity = 10
price = 1550

rqStatus = utils.makeStockSellOrder(code, quantity, price)

if rqStatus != 0:
    exit()
