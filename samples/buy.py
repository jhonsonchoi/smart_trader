import utils

utils.InitPlusCheck()

# 주식 매수 주문
# A900290
code = "A900290"
quantity = 10
price = 1520

rqStatus = utils.makeStockBuyOrder(code, quantity, price)

if rqStatus != 0:
    exit()

