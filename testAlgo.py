import sys
import time
import random
import win32com.client
from PyQt5.QtWidgets import *
from cybos import traders

# code = 'A122630'  # kodex leverage
# code = 'A252670'  # kodex inverse 2x


class Pricer:
    timeout = 10000
    price = 10000
    prev = 0
    tick = 0

    def __init__(self, code, trader):
        self.code = code
        self.trader = trader

    def fetchPrice(self):
        self.tick = self.tick + 1
        curr = random.randrange(0, sys.maxsize)

        if curr < self.prev:
            newPrice = self.price - 100
            if newPrice < 500:
                newPrice = 500
            self.price = newPrice

        if curr > self.prev:
            newPrice = self.price + 100
            self.price = newPrice

        self.prev = curr

        return self.price

    def run(self):
        while self.tick < self.timeout:
            p = self.fetchPrice()
            trader.buyOrSell(self.tick, self.price)



class Pricer2:
    tick = 0

    def __init__(self, code, trader):
        self.code = code
        self.trader = trader

    # 종목 코드
    # code = 'A005930'  # 삼성전자
    # code = 'A122630'  # kodex leverage
    # code = 'A252670'  # kodex inverse 2x
    # code = "A900290"  # GRT
    def fetchPrice(self):
        self.tick = self.tick + 1

        # 연결 여부 체크
        objCpCybos = win32com.client.Dispatch("CpUtil.CpCybos")
        bConnect = objCpCybos.IsConnect
        if (bConnect == 0):
            print("PLUS가 정상적으로 연결되지 않음. ")
            exit()

        # 현재가 객체 구하기
        objStockMst = win32com.client.Dispatch("DsCbo1.StockMst")
        objStockMst.SetInputValue(0, self.code)
        objStockMst.BlockRequest()

        # 현재가 통신 및 통신 에러 처리
        rqStatus = objStockMst.GetDibStatus()
        rqRet = objStockMst.GetDibMsg1()
        # print("통신상태", rqStatus, rqRet)
        if rqStatus != 0:
            print("통신상태", rqStatus, rqRet)
            exit()

        # int
        return objStockMst.GetHeaderValue(16)






# A005930 samsung
# A122630 kodex lev
# A900290 grt
# objStockCur = CpStockCur()
# objStockCur.Subscribe('A122630')


inv = traders.Inventory()
trader = traders.Trader(inv)
pricer = Pricer('A122630', trader)
pricer.run()


# self.StopSubscribe()

# app = QApplication(sys.argv)
# myWindow = MyWindow()
# myWindow.show()

# app.exec_()
