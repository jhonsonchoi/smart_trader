a = 0

def g_func():
    print('g_func:', a)


class G:
    a = 2
    def l_func(self):
        print('l_func:', self.a)
        g_func()
