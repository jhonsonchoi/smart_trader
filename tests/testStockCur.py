import sys
import threading
from PyQt5.QtWidgets import *
from cybos import stockcur
from cybos import marketeye
from cybos import brokers
from cybos import traders


codes = ['A122630', 'A252670', 'A011000']  # 'A122630', 'A252670'
profit_rate = 32
buyGapStep = 30  # Trader 설정

# Broker
# broker = inventory.RealBroker(code, quantity, quantity * average)
# Trader
# trader = algo.Trader(broker, profit_rate, stocksPerBuy)


class MyWindow(QMainWindow):
    # 실시간 시세 요청중 플래그
    isSB = False
    # 코드별 CpStockCur 목록
    objCur = []

    def __init__(self):
        super().__init__()
        print('MyWindow')
        self.setWindowTitle("PLUS API TEST")
        self.setGeometry(300, 300, 300, 180)
        self.isSB = False
        self.objCur = []

        btnStart = QPushButton("요청 시작", self)
        btnStart.move(20, 20)
        btnStart.clicked.connect(self.btnStart_clicked)

        btnStop = QPushButton("요청 종료", self)
        btnStop.move(20, 70)
        btnStop.clicked.connect(self.btnStop_clicked)

        btnExit = QPushButton("종료", self)
        btnExit.move(20, 120)
        btnExit.clicked.connect(self.btnExit_clicked)

    def StopSubscribe(self):
        if self.isSB:
            cnt = len(self.objCur)
            for i in range(cnt):
                self.objCur[i].Unsubscribe()
            print(cnt, "종목 실시간 해지되었음")
        self.isSB = False

        self.objCur = []

    def btnStart_clicked(self):
        self.StopSubscribe()
        # codes = []
        # 잔고조회
        # obj6033 = inventory.Cp6033()
        # if obj6033.Request(codes) == False:
        #     return
        #
        # print("잔고 종목 개수:", len(codes))

        # 요청 필드 배열 - 종목코드, 시간, 대비부호 대비, 현재가, 거래량, 종목명
        # rqField = [0, 1, 2, 3, 4, 10, 17]  # 요청 필드
        # objMarkeyeye = marketeye.CpMarketEye()
        # if (objMarkeyeye.Request(codes, rqField) == False):
        #     exit()

        obj6033 = brokers.Cp6033()
        data = []
        if obj6033.my_rq6033(data) == False:
            exit()

        # A122630 1032 53 -335 14230  32481697 KODEX 레버리지
        # A252670 1032 50  105  4345 113628178 KODEX 200 선물인

        # codes = [code]  # 'A122630' 레버 'A252670' 인버스 레버 'A900290' GRT
        cnt = len(codes)
        for i in range(cnt):
            quantity = 0
            price = average = 0
            stocksPerBuy = 0

            for j in data:
                if codes[i] == j[0]:
                    quantity = j[1]
                    price = average = int(j[2])
                    stocksPerBuy = 3 if average < 10000 else 1

            print("code={} quantity={} price={} average={} stocksPerBuy={}".format(codes[i], quantity, price, average, stocksPerBuy))
            broker = brokers.RealBroker(codes[i], quantity, quantity * average)
            self.objCur.append(stockcur.CpStockCur(traders.SimpleTrader(codes[i], broker, profit_rate, stocksPerBuy, price)))
            self.objCur[i].Subscribe(codes[i])

        print("빼기빼기================-")
        print(cnt, "종목 실시간 현재가 요청 시작")
        self.isSB = True

    def btnStop_clicked(self):
        self.StopSubscribe()

    def btnExit_clicked(self):
        self.StopSubscribe()
        exit()


if __name__ == "__main__":
    print('main thread=', threading.get_ident())
    app = QApplication(sys.argv)
    myWindow = MyWindow()
    myWindow.show()
    app.exec_()


# C:\Anaconda3\python.exe C:/Users/jhonson/PycharmProjects/smart_trader/samples/testStockCur.py
# 782191776 01
# 통신상태 0 12247 조회완료되었습니다.(cii.cif.astbln.selcnsbaseaccpt)
# 10
# 종목코드 종목명 신용구분 체결잔고수량 체결장부단가 평가금액 평가손익
# A030200 KT 32 819 24165.5298 19116000 -3.4099999999999966
# A039490 키움증권 32 60 111678.0334 6463000 -3.5400000000000063
# A086790 하나금융지주 32 336 29369.8125 9535000 -3.3700000000000045
# A105560 KB금융 32 507 38933.3668 19116000 -3.1500000000000057
# A123890 한국자산신탁 32 2682 3673.2445 9764000 -0.8799999999999955
# A138930 BNK금융지주 32 1881 5245.5327 9625000 -2.450000000000003
# A334890 이지스밸류리츠 32 420 4473.6262 1874000 -0.21999999999999886
# A338100 NH프라임리츠 32 1149 4247.612 4830000 -1.019999999999996
# A350520 이지스레지던스리츠 32 420 4477.7295 1952000 3.8100000000000023
# A900290 GRT 32 6483 1544.7494 9537000 -4.760000000000005
# A030200
# A039490
# A086790
# A105560
# A123890
# A138930
# A334890
# A338100
# A350520
# A900290
# 잔고 종목 개수: 10
# 통신상태 0 0027 조회가 완료되었습니다.(market.eye)
# A030200 926 50 100 23400 111840 KT
# A039490 926 53 -500 108000 3801 키움증권
# A086790 926 51 0 28450 111774 하나금융지주
# A105560 926 53 -200 37750 176610 KB금융
# A123890 926 51 0 3645 19727 한국자산신탁
# A138930 926 53 -10 5130 168145 BNK금융지주
# A334890 926 50 5 4475 2463 이지스밸류리츠
# A338100 926 50 5 4215 3443 NH프라임리츠
# A350520 926 50 10 4660 128 이지스레지던스리
# A900290 926 50 5 1475 39750 GRT
# 빼기빼기================-
# 10 종목 실시간 현재가 요청 시작
# 실시간(장중 체결) KB금융 92635 37750 대비 -200 체결량 2 거래량 176612 매도호가 37800 매수호가 37750
# 실시간(장중 체결) KT 92635 23400 대비 100 체결량 115 거래량 111955 매도호가 23400 매수호가 23350
# 실시간(장중 체결) 하나금융지주 92636 28400 대비 -50 체결량 10 거래량 111784 매도호가 28450 매수호가 28400
# 실시간(장중 체결) KT 92636 23400 대비 100 체결량 1 거래량 111956 매도호가 23400 매수호가 23350
# 실시간(장중 체결) KB금융 92637 37750 대비 -200 체결량 100 거래량 176712 매도호가 37800 매수호가 37750
# 실시간(장중 체결) KT 92638 23400 대비 100 체결량 10 거래량 111966 매도호가 23400 매수호가 23350
# 실시간(장중 체결) KT 92639 23400 대비 100 체결량 5 거래량 111971 매도호가 23400 매수호가 23350
# 실시간(장중 체결) KB금융 92642 37750 대비 -200 체결량 4 거래량 176716 매도호가 37800 매수호가 37750
# 실시간(장중 체결) KB금융 92645 37750 대비 -200 체결량 118 거래량 176834 매도호가 37800 매수호가 37750
# 실시간(장중 체결) BNK금융지주 92645 5130 대비 -10 체결량 231 거래량 168376 매도호가 5140 매수호가 5130
# 실시간(장중 체결) 한국자산신탁 92645 3645 대비 0 체결량 1 거래량 19728 매도호가 3650 매수호가 3645
# 실시간(장중 체결) KB금융 92645 37750 대비 -200 체결량 990 거래량 177824 매도호가 37800 매수호가 37750
# 실시간(장중 체결) BNK금융지주 92646 5130 대비 -10 체결량 1 거래량 168377 매도호가 5140 매수호가 5130
# 실시간(장중 체결) BNK금융지주 92647 5130 대비 -10 체결량 210 거래량 168587 매도호가 5140 매수호가 5130
# 실시간(장중 체결) 하나금융지주 92648 28450 대비 0 체결량 27 거래량 111811 매도호가 28450 매수호가 28400
# 실시간(장중 체결) KB금융 92648 37800 대비 -150 체결량 71 거래량 177895 매도호가 37800 매수호가 37750
# 10 종목 실시간 해지되었음
#
# Process finished with exit code 0
