

class Parent:
    def do(self):
        print('Parent')


class Child(Parent):
    def do(self):
        print('Child')


p = Parent()
p.do()
print(type(p))

p = Child()
p.do()
print(type(p))
