import sys
import time
import random

class Pricer:
    price = 10000
    prev = 0

    def fetchPrice(self):
        curr = random.randrange(0, sys.maxsize)

        if curr < self.prev:
            newPrice = self.price - 100
            self.price = newPrice

        if curr > self.prev:
            newPrice = self.price + 100
            self.price = newPrice

        self.prev = curr

        return self.price


x = 0
prevPrice = sys.maxsize
direction = 0
contTimes = 0
stocks = 0
cost = 0
average = 0
profit = 0
buyPrice = 0

pricer = Pricer()

while True:
    # time.sleep(1)

    # print('----------------------------')

    currentPrice = pricer.fetchPrice()

# 매도 전술
    if currentPrice < prevPrice:
        if direction == -1:
            # 연속 하락이면
            contTimes = contTimes + 1
        else:
            # 하락으로 바뀌었으면
            direction = -1
            contTimes = 0

        gap = int((buyPrice - currentPrice) / 100)

        # print(f'currentPrice={currentPrice:10d}, direction={direction:10d}, contTimes={contTimes:10d}')

        # 연속 하락이면
        if (gap > 1 or contTimes > 0) and stocks > 1:
            count = contTimes + 1
            if count > stocks:
                count = stocks

            print(f"sell: count={count:2d}, gap={gap:2d}, contTimes={contTimes:2d}")
            stocks = stocks - count
            cost = stocks * average
            profit = profit + count * (currentPrice - average)
            print(f'stocks={stocks:10d}, cost={cost:10d}, average={average:10d}, profit={profit:10d}')

# 매수 전술
    if currentPrice > prevPrice:
        if direction == 1:
            # 연속 상승이면
            contTimes = contTimes + 1
        else:
            # 상승으로 바뀌었으면
            direction = 1
            contTimes = 0

        # print(f'currentPrice={currentPrice:10d}, direction={direction:10d}, contTimes={contTimes:10d}')

        # 매수
        if contTimes > 1:
            buyPrice = currentPrice
            count = 1
            print(f"buy: count={count:2d}")
            stocks = stocks + count
            cost = cost + count * currentPrice
            average = int(cost / stocks)
            print(f'stocks={stocks:10d}, cost={cost:10d}, average={average:10d}, profit={profit:10d}')

    prevPrice = currentPrice

