

class Trader:
    h_tick, h_cprice, h_sell, h_buy = 0, 0, 0, 0

    def __init__(self, code, broker) -> None:
        super().__init__()
        self.code = code
        self.broker = broker

    def print_stat(self, broker, tick, sell, buy, direction, contTimes):
        print('Trader: tick=', tick, 'sell=', sell, 'buy=', buy, 'direction=', direction, 'contTimes=', contTimes, 'quantity=', broker.quantity, 'cost=', broker.cost, 'average=', broker.average(), 'profit=', broker.profit)

    def trade(self, tick, cprice, sell, buy):
        if self.h_sell == sell and self.h_buy == buy:
            return
        else:
            self.h_tick = tick
            self.h_cprice = cprice
            self.h_sell = sell
            self.h_buy = buy
        # datetime.time.datetime(tick)
        print(tick)


class SimpleTrader(Trader):
    prevSell = 0
    prevBuy = 0
    sellDirection = 0
    sellContTimes = 0
    buyDirection = 0
    buyContTimes = 0

    buyGapStep = 10

    quantity_per_buy = 0
    max_quantity = 100

    target_profit = 50
    stop_loss = -500

    h_tick, h_cprice, h_sell, h_buy = 0, 0, 0, 0

    def __init__(self, code, broker, target_profit, quantity_per_buy, price) -> None:
        super().__init__(code, broker)
        self.target_profit = target_profit
        self.quantity_per_buy = quantity_per_buy
        self.max_quantity = quantity_per_buy * 50
        self.prevSell = self.prevBuy = price

    def print_stat(self, broker, tick, sell, buy, direction, contTimes):
        print('Trader: tick=', tick, 'sell=', sell, 'buy=', buy, 'direction=', direction, 'contTimes=', contTimes, 'quantity=', broker.quantity, 'cost=', broker.cost, 'average=', broker.average(), 'profit=', broker.profit)

    def trade(self, tick, cprice, sell, buy):
        """
        - 매도호가가 1% 이상 높아지면 매수: 기준가는 거래이후 제일 낮은 매도호가\n
        - 매수호가가 1% 이상 낮아지면 매도: 기준가는 거래이후 제일 높은 매수호가\n
        - 최대로 매수한 상태에서 매수호가가 보유가보다 너무 내려가면 손절함\n
        \n
        매도호가와 매수호가가 서로 벌어져 있다가 아래위로 좁아지면 매수매도가 동시에 일어날 수 있음. 단, 현실적으로는 불가능해보임.\n

        :param tick: hhmm 형식의 시각
        :param cprice: 거래가
        :param sell: 매도호가
        :param buy: 매수호가
        :return:
        """
        # 똑같은 주문 발생 방지
        if self.h_sell == sell and self.h_buy == buy:
            return
        else:
            self.h_tick = tick
            self.h_cprice = cprice
            self.h_sell = sell
            self.h_buy = buy

        # 기준가 초기화
        if self.prevSell == 0:
            self.prevSell = cprice

        # 기준가 초기화
        if self.prevBuy == 0:
            self.prevBuy = cprice

        # 매수단위수량 초기화
        if self.quantity_per_buy == 0:
            quantity_per_buy = int(10000 / cprice) + 1
            self.quantity_per_buy = quantity_per_buy
            self.max_quantity = quantity_per_buy * 50

        # 신호 찾기
        # print('currentPrice', currentPrice, 'prevPrice', self.prevPrice)

        # 매도호가 기준
        if sell > self.prevSell:
            self.sellDirection = 1
            self.sellContTimes = int((sell - self.prevSell) / self.prevSell * 10000)

        if sell < self.prevSell:
            self.sellDirection = -1
            self.sellContTimes = int((self.prevSell - sell) / self.prevSell * 10000)

        if sell == self.prevSell:
            self.sellContTimes = 0

        # 매수호가 기준
        if buy > self.prevBuy:
            self.buyDirection = 1
            self.buyContTimes = int((buy - self.prevBuy) / self.prevBuy * 10000)

        if buy < self.prevBuy:
            self.buyDirection = -1
            self.buyContTimes = int((self.prevBuy - buy) / self.prevBuy * 10000)

        if buy == self.prevBuy:
            self.buyContTimes = 0

        # 거래 제한 조건 구하기
        # gap 을 구함. 1만분의 1단위
        average = self.broker.average()
        if average == 0:
            buy_gap = 0
        else:
            buy_gap = int((buy - average) / average * 10000)

        broker_quantity = self.broker.quantity

        sell_dir_str = buy_dir_str = ""
        if self.sellDirection > 0:
            sell_dir_str = "+"
        if self.sellDirection < 0:
            sell_dir_str = "-"
        if self.buyDirection > 0:
            buy_dir_str = "+"
        if self.buyDirection < 0:
            buy_dir_str = "-"

        print("Trader: ", tick, self.code, "cprice=", cprice, sell, buy, "/ average=", average, ", broker_quantity=", broker_quantity, "/ bgap=", buy_gap, "/", "sellContTimes= {}{}, buyContTimes= {}{}".format(sell_dir_str, self.sellContTimes, buy_dir_str, self.buyContTimes))

        # 주요 변수
        # self.sell_direction:   방향 +1: 상승, -1: 하락
        # self.sell_confTimes:   비율(1/10000)
        # self.buy_direction:   방향 +1: 상승, -1: 하락
        # self.buy_confTimes:   비율(1/10000)
        # sell:             매도호가
        # buy:              매수호가
        # buy_gap:          매수호가와 보유가의 차이

        # 매도(익절)
        # 매수호가가 1% 이상 낮아지면
        if self.buyDirection == -1 and self.buyContTimes >= 100:
            # 매도
            # 매수호가와 보유가 차이가 목표이익보다 충분히 높으면 매도
            if buy_gap > self.target_profit:
                # 수량 결정
                broker_quantity = self.broker.quantity
                quantity = 1 if broker_quantity == 1 else int(broker_quantity / 2)

                # 매도 실행
                print("Trader: sell: tick={} dist={} buy_gap={} target_profit={} price={} quantity={}".format(tick, self.buyContTimes, buy_gap, self.target_profit, buy, quantity))
                self.broker.sell(quantity, buy)
                self.prevSell = self.prevBuy = buy

        # 매수
        # 매도호가가 1% 이상 높아지면
        if self.sellDirection == 1 and self.sellContTimes >= 100:
            if self.broker.quantity + self.quantity_per_buy <= self.max_quantity:
                quantity = self.quantity_per_buy
                print("Trader: buy: tick={} dist={} price={} quantity={}".format(tick, self.sellContTimes, sell, quantity))
                self.broker.buy(quantity, sell)
                self.prevSell = self.prevBuy = sell

        # 매도(손절)
        # 최대보유량인 상태에서 보유단가보다 일정수준 아래로 내려가면
        if self.broker.quantity > self.max_quantity - self.quantity_per_buy and buy_gap <= self.stop_loss:
            quantity = int(self.broker.quantity / 2)
            print("Trader: stop loss: tick={} buy_gap={} stop_loss={} price={} quantity={}".format(tick, buy_gap, self.stop_loss, buy, quantity))
            self.broker.sell(quantity, buy)
            self.prevSell = self.prevBuy = buy

        # 매도호가 매수호가 기준가 변경
        if self.prevBuy != self.prevSell:
            if sell < self.prevSell:
                self.prevSell = sell
            if buy > self.prevBuy:
                self.prevBuy = buy

        pass
