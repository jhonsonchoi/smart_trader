from cybos import comm


# CpStockCur: 실시간 현재가 요청 클래스
class CpStockCur:
    """
    StockCur 와 EventHandler 매칭
    """

    def __init__(self, trader) -> None:
        super().__init__()
        print('CpStockCur')
        self.trader = trader

    def Subscribe(self, code):
        self.objStockCur = comm.StockCur()
        handler = comm.buildHandler(self.objStockCur, CpEvent)
        self.objStockCur.SetInputValue(0, code)
        handler.set_params(self.objStockCur)
        handler.set_trader(self.trader)
        self.objStockCur.Subscribe()

    def Unsubscribe(self):
        self.objStockCur.Unsubscribe()


# CpEvent: 실시간 이벤트 수신 클래스
class CpEvent:
    """client: DsCbo1.StockCur 오브젝트임

    """
    def set_params(self, client):
        self.client = client

    def set_trader(self, trader):
        self.trader = trader

    def OnReceived(self):
        code = self.client.GetHeaderValue(0)  # 초
        name = self.client.GetHeaderValue(1)  # 초
        timess = self.client.GetHeaderValue(18)  # 초
        exFlag = self.client.GetHeaderValue(19)  # 예상체결 플래그
        cprice = self.client.GetHeaderValue(13)  # 현재가
        diff = self.client.GetHeaderValue(2)  # 대비
        cVol = self.client.GetHeaderValue(17)  # 순간체결수량
        vol = self.client.GetHeaderValue(9)  # 거래량
        sell = self.client.GetHeaderValue(7)  # 매도호가
        buy = self.client.GetHeaderValue(8)  # 매수호가

        # if exFlag == ord('1'):  # 동시호가 시간 (예상체결)
        #     self.trader.trade(code, name, timess, cprice, diff, cVol, vol, sell, buy)
        if exFlag == ord('2'):  # 장중(체결)
            self.trader.trade(timess, cprice, sell, buy)

