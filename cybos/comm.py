import win32com.client


def CpCybos():
    """CpUtil.CpCybos
    연결
    :return:
    """
    return win32com.client.Dispatch("CpUtil.CpCybos")


# 계좌 정보
def CpTrade():
    """CpTrade.CpTdUtil

    계좌 정보
    :return:
    """
    return win32com.client.Dispatch('CpTrade.CpTdUtil')


def StockOrder():
    """CpTrade.CpTd0311

    주문
    :return:
    """
    return win32com.client.Dispatch("CpTrade.CpTd0311")


def CpTd6033():
    """CpTrade.CpTd6033

    주문
    :return:
    """
    return win32com.client.Dispatch("CpTrade.CpTd6033")


def StockCur():
    """DsCbo1.StockCur
    종목시세 관찰
    :return:
    """
    return win32com.client.Dispatch("DsCbo1.StockCur")


def MarketEye():
    """CpSysDib.MarketEye
    관심종목
    :return:
    """
    return win32com.client.Dispatch("CpSysDib.MarketEye")

def buildHandler(StockCur, EventClass):
    """

    :param StockCur:
    :param EventClass:
    :return:
    """
    return win32com.client.WithEvents(StockCur, EventClass)