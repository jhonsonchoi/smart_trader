from cybos import comm


class Broker:
    code = ""
    quantity = 0
    cost = 0
    profit = 0

    def __init__(self, code, quantity, cost) -> None:
        super().__init__()
        self.code = code
        self.quantity = quantity
        self.cost = cost

    def sell(self, quantity, price):
        self.profit += (price - self.average()) * quantity
        self.quantity -= quantity
        self.cost -= quantity * price

    def buy(self, quantity, price):
        self.quantity += quantity
        self.cost += quantity * price

    def average(self):
        if self.quantity == 0:
            return 0
        else:
            return int(self.cost / self.quantity)

    def print(self):
        print("Broker: stocks={} average={} cost={} profit={}".format(self.quantity, self.average(), self.cost, self.profit))


class RealBroker(Broker):
    # code = ''
    # stocks = 0
    # cost = 0
    # maxStocks = 100
    # profit = 0

    def __init__(self, code, quantity, cost) -> None:
        super().__init__(code, quantity, cost)
        # self.code = code

    def sell(self, quantity, price):
        super().sell(quantity, price)
        # 주식 매도 주문
        g_objCpTrade = comm.CpTrade()
        g_objCpTrade.TradeInit()
        acc = g_objCpTrade.AccountNumber[0]  # 계좌번호
        accFlag = g_objCpTrade.GoodsList(acc, 1)  # 주식상품 구분
        objStockOrder = comm.StockOrder()
        objStockOrder.SetInputValue(0, "1")  # 1: 매도
        objStockOrder.SetInputValue(1, acc)  # 계좌번호
        objStockOrder.SetInputValue(2, accFlag[0])  # 상품구분 - 주식 상품 중 첫번째
        objStockOrder.SetInputValue(3, self.code)  # 종목코드 - A003540 - 대신증권 종목
        objStockOrder.SetInputValue(4, quantity)  # 매도수량 10주
        objStockOrder.SetInputValue(5, price)  # 주문단가  - 14,100원
        objStockOrder.SetInputValue(7, "0")  # 주문 조건 구분 코드, 0: 기본 1: IOC 2:FOK
        objStockOrder.SetInputValue(8, "01")  # 주문호가 구분코드 - 01: 보통

        # 매도 주문 요청
        objStockOrder.BlockRequest()

        rqStatus = objStockOrder.GetDibStatus()
        rqRet = objStockOrder.GetDibMsg1()

        print("통신상태", rqStatus, rqRet)

    def buy(self, quantity, price):
        # if self.code == 'A252670':
        #     return
        super().buy(quantity, price)
        # 주식 매수 주문
        g_objCpTrade = comm.CpTrade()
        g_objCpTrade.TradeInit()
        acc = g_objCpTrade.AccountNumber[0]  # 계좌번호
        accFlag = g_objCpTrade.GoodsList(acc, 1)  # 주식상품 구분
        objStockOrder = comm.StockOrder()
        objStockOrder.SetInputValue(0, "2")  # 2: 매수
        objStockOrder.SetInputValue(1, acc)  # 계좌번호
        objStockOrder.SetInputValue(2, accFlag[0])  # 상품구분 - 주식 상품 중 첫번째
        objStockOrder.SetInputValue(3, self.code)  # 종목코드 - A003540 - 대신증권 종목
        objStockOrder.SetInputValue(4, quantity)  # 매수수량 10주
        objStockOrder.SetInputValue(5, price)  # 주문단가  - 14,100원
        objStockOrder.SetInputValue(7, "0")  # 주문 조건 구분 코드, 0: 기본 1: IOC 2:FOK
        objStockOrder.SetInputValue(8, "01")  # 주문호가 구분코드 - 01: 보통

        # 매수 주문 요청
        objStockOrder.BlockRequest()

        rqStatus = objStockOrder.GetDibStatus()
        rqRet = objStockOrder.GetDibMsg1()

        print("통신상태", rqStatus, rqRet)

    # def average(self):
    #     g_objCpTrade = comm.CpTrade()
    #     acc = g_objCpTrade.AccountNumber[0]  # 계좌번호
    #     accFlag = g_objCpTrade.GoodsList(acc, 1)  # 주식상품 구분
    #     objRq = comm.CpTd6033()
    #     objRq.SetInputValue(0, acc)  # 계좌번호
    #     objRq.SetInputValue(1, accFlag[0])  # 상품구분 - 주식 상품 중 첫번째
    #     objRq.SetInputValue(2, 50)  # 요청 건수(최대 50)
    #     objRq.BlockRequest()
    #
    #     # 통신 및 통신 에러 처리
    #     rqStatus = objRq.GetDibStatus()
    #     rqRet = objRq.GetDibMsg1()
    #     print("통신상태", rqStatus, rqRet)
    #     if rqStatus != 0:
    #         return False
    #
    #     cnt = objRq.GetHeaderValue(7)
    #     print(cnt)
    #
    #     retPrice = 0
    #     for i in range(cnt):
    #         code = objRq.GetDataValue(12, i)  # 종목코드
    #         name = objRq.GetDataValue(0, i)  # 종목명
    #         # retcode.append(code)
    #         # if len(retcode) >= 200:  # 최대 200 종목만,
    #         #     break
    #         cashFlag = objRq.GetDataValue(1, i)  # 신용구분
    #         date = objRq.GetDataValue(2, i)  # 대출일
    #         amount = objRq.GetDataValue(7, i)  # 체결잔고수량
    #         buyPrice = objRq.GetDataValue(17, i)  # 체결장부단가
    #         evalValue = objRq.GetDataValue(9, i)  # 평가금액(천원미만은 절사 됨)
    #         evalPerc = objRq.GetDataValue(11, i)  # 평가손익
    #         if self.code == code:
    #             retPrice = buyPrice
    #
    #     return retPrice
    #
    # def print(self):
    #     print('stocks=', self.stocks, 'cost=', self.cost, 'average=', self.average(), 'profit=', self.profit)


# Cp6033 : 주식 잔고 조회
class Cp6033:
    def __init__(self):
        # 통신 OBJECT 기본 세팅
        self.objTrade = comm.CpTrade()
        initCheck = self.objTrade.TradeInit(0)
        if (initCheck != 0):
            print("주문 초기화 실패")
            return

        #
        acc = self.objTrade.AccountNumber[0]  # 계좌번호
        accFlag = self.objTrade.GoodsList(acc, 1)  # 주식상품 구분
        print(acc, accFlag[0])

        self.objRq = comm.CpTd6033()
        self.objRq.SetInputValue(0, acc)  # 계좌번호
        self.objRq.SetInputValue(1, accFlag[0])  # 상품구분 - 주식 상품 중 첫번째
        self.objRq.SetInputValue(2, 50)  # 요청 건수(최대 50)

    # 실제적인 6033 통신 처리
    def rq6033(self, retcode):
        self.objRq.BlockRequest()

        # 통신 및 통신 에러 처리
        rqStatus = self.objRq.GetDibStatus()
        rqRet = self.objRq.GetDibMsg1()
        print("통신상태", rqStatus, rqRet)
        if rqStatus != 0:
            return False

        cnt = self.objRq.GetHeaderValue(7)
        print(cnt)

        print("종목코드 종목명 신용구분 체결잔고수량 체결장부단가 평가금액 평가손익")
        for i in range(cnt):
            code = self.objRq.GetDataValue(12, i)  # 종목코드
            name = self.objRq.GetDataValue(0, i)  # 종목명
            retcode.append(code)
            if len(retcode) >= 200:  # 최대 200 종목만,
                break
            cashFlag = self.objRq.GetDataValue(1, i)  # 신용구분
            date = self.objRq.GetDataValue(2, i)  # 대출일
            amount = self.objRq.GetDataValue(7, i)  # 체결잔고수량
            addedBuyPrice = self.objRq.GetDataValue(4, i)  # 결제장부단가
            buyPrice = self.objRq.GetDataValue(17, i)  # 체결장부단가
            evalValue = self.objRq.GetDataValue(9, i)  # 평가금액(천원미만은 절사 됨)
            evalPerc = self.objRq.GetDataValue(11, i)  # 평가손익

            print(code, name, cashFlag, amount, buyPrice, evalValue, evalPerc, addedBuyPrice)

    # 실제적인 6033 통신 처리
    def my_rq6033(self, retcode):
        self.objRq.BlockRequest()

        # 통신 및 통신 에러 처리
        rqStatus = self.objRq.GetDibStatus()
        rqRet = self.objRq.GetDibMsg1()
        print("통신상태", rqStatus, rqRet)
        if rqStatus != 0:
            return False

        cnt = self.objRq.GetHeaderValue(7)
        print(cnt)

        print("종목코드 종목명 신용구분 체결잔고수량 체결장부단가 평가금액 평가손익")
        for i in range(cnt):
            code = self.objRq.GetDataValue(12, i)  # 종목코드
            name = self.objRq.GetDataValue(0, i)  # 종목명
            # retcode.append(code)
            # if len(retcode) >= 200:  # 최대 200 종목만,
            #     break
            cashFlag = self.objRq.GetDataValue(1, i)  # 신용구분
            date = self.objRq.GetDataValue(2, i)  # 대출일
            amount = self.objRq.GetDataValue(7, i)  # 체결잔고수량
            addedBuyPrice = self.objRq.GetDataValue(4, i)  # 결제장부단가
            buyPrice = self.objRq.GetDataValue(17, i)  # 체결장부단가
            evalValue = self.objRq.GetDataValue(9, i)  # 평가금액(천원미만은 절사 됨)
            evalPerc = self.objRq.GetDataValue(11, i)  # 평가손익

            retcode.append((code, amount, buyPrice))

            print(code, name, cashFlag, amount, buyPrice, evalValue, evalPerc, addedBuyPrice)

    def Request(self, retCode):
        self.rq6033(retCode)

        # 연속 데이터 조회 - 200 개까지만.
        while self.objRq.Continue:
            self.rq6033(retCode)
            print(len(retCode))
            if len(retCode) >= 200:
                break
        # for debug
        size = len(retCode)
        for i in range(size):
            print(retCode[i])
        return True

